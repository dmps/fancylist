function fancyList(...items) {
    var context = this
    this.checkConsistentType = (itemsArr) => {
        return itemsArr.reduce((type, item) => {
            if (!type) return typeof item
            if (type !== typeof item) {
                throw new Error(`Fancylist is not of type ${typeof item}`)
                return false
            } else {
                return type
            }
        }, '');
    }
    //no arguements
    if (items === []) {
        context.type = 'undefined';
        context.items = []
    }
    //an array
    else if (typeof items[0] === "object") {
        let arr = []
        for (let i = 0; i < items[0].length; i++) {
            arr.push(items[0][i])
        }
        context.type = context.checkConsistentType(arr)
        context.items = arr
    }
    //a list
    else {
        context.type = context.checkConsistentType(items)
        context.items = items
    }
}
fancyList.prototype.getItemAt = function (index) {
    return this.items[index]
}
fancyList.prototype.getItemsAt = function (index, numItems) {
    return this.items.substring(index, index + numItems)
}

fancyList.prototype.addItem = function (item) {
    if (typeof item === this.type) {
        this.items.push(item);
        return this;
    }
    throw new Error(`fancyList is not of type ${typeof item}`);
}
fancyList.prototype.addItems = function (...items) {
    //check for array
    if (items.length === 1 && typeof items[0] === 'object') {
        items.forEach((item) => {
            for (let i = 0; i < items[0].length; i++) {
                this.items.push(items[0][i])
            }
        })
        return this
    } else {
        items.forEach((item) => {
            if (typeof item === this.type) {
                this.items.push(item)
            } else {
                throw new Error(`fancyList is not of type ${typeof item}`);
            }
        }, this)
        return this
    }
}

fancyList.prototype.insertItemAt = function (index, item) {
    if (typeof item !== this.type) throw new Error(`fancyList is not of type ${typeof item}`);
    this.items = [...this.items.slice(0, index), item, ...this.items.slice(index)]
    return this.items
}

fancyList.prototype.insertItemsAt = function (...args) {
    if (args.length === 2 && typeof args[1] === "object") {
        for (let i = 0; i < args[1].length; i++) {
            if (typeof args[1][i] !== this.type) throw new Error(`fancyList is not of type ${typeof args[1][i]}`);
        }
        this.items = [...this.items.slice(0, args[0]), ...args[1], ...this.items.slice(args[0])]
    } else {
        for (let i = 0; i < args.length; i += 2) {
            if (typeof args[i + 1] !== this.type) throw new Error(`fancyList is not of type ${typeof args[i+1]}`);
            this.items = [...this.items.slice(0, args[i]), args[i + 1], ...this.items.slice(args[i])]
        }
    }
    return this
}

fancyList.prototype.removeItemAt = function (index) {
    if (-1 > index || index > this.items.length || typeof index!== 'number') throw new Error('Invalid index');
    this.items.splice(index, 1)
    return this
}

fancyList.prototype.removeItemsAt = function (index, numItems) {
    if (-1 > index || index > this.items.length || typeof index!== 'number') throw new Error('Invalid index');
    this.items.splice(index, numItems)
    return this
}

fancyList.prototype.removeItem = function(item) {
    for(var i=0;i<this.items.length;i++){
        if(this.items[i]===item) {
            this.removeItemAt(i)
            break;
        }
    }
    return this
}

fancyList.prototype.removeItems = function(...args){
    //check for array
    if(args.length==1&& typeof args[0]==='object'){
        for(var i=0;i<args[0].length;i++){
            this.removeItem(args[0][i])
        }
    }else{
        for(var i=0;i<args.length;i++){
            this.removeItem(args[i])
        }
    }
    return this
}


// Some dummy data for your testing. All of the functions are chainable so go wild :)
//The safe data is all stored in this.items
// const emptyList = new fancyList();
// const arrList = new fancyList([3, 4, 5, 6]);
// const listList = new fancyList(4, 5, 2, 2);
