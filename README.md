# FancyList Coding Challenge

I built a JS prototype called "FancyList" that simulates a list and provides all methods and properties to manage the list and retrieving vital information about the list. The list secretly hosts an Array which is private to the FancyList object. This array is the actual storage of FancyList. The List also forbids the use of multiple types in the same list. Hence, when adding items to FancyList for the first time, the object introspects the items and the 1st item determines the type.

Eg:

If items are [1,2,3,4,5] then FancyList is restricted to Number.

If I did, myList.add('name');, it throws an Error, because 'name' is a String.

As such, I also cannot do myList.add([1,2,3,'name']);

Once the type is established, FancyList always checks that the correct type is being used when adding/inserting elements in the list.

Eg: I cannot create a FancyList for 'number' s, then decide to add a boolean as my next item.

---

## Constructor

The constructor is flexible. It allows:
1. Passing no items
1. Passing 1 item
1. Passing many items (comma delimited)
1. Passing many items (as an Array)

Eg:

var myList1 = new FancyList();

var myList2 = new FancyList(3);

var myList3 = new FancyList(3, 4);

var myList4 = new FancyList([5, 6]);

Methods are:

new fancyList (see above)

getItemAt(index); (0 is 1st item)

getItemsAt(index, numberOfItems);

addItem(item);

addItems(item1, ...); (accepts 1 item too)

addItems([item1, item2, ... itemN]);

insertItemAt(index, item, index); (index is where the item is inserted, i.e, the position)

insertItemsAt(index, item1, ...);

insertItemsAt(index, [item1, item2, ... itemN]);

removeItemAt(index);

removeItemsAt(index, numberOfItems);

removeItem(item); (if there are duplicates, should remove 1st item found)

removeItems(item1, ...); (accepts 1 item too)

removeItems([item1, item2, ... itemN]);
